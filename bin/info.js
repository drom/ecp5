#!/usr/bin/env node
'use strict';

const usb = require('usb');
const lib = require('../lib/');

const lus = {
    LIBUSB_REQUEST_TYPE_VENDOR: (0x02 << 5),
    LIBUSB_RECIPIENT_DEVICE: 0,
    LIBUSB_ENDPOINT_IN: 0x80,
    LIBUSB_ENDPOINT_OUT: 0
};

const ftd = {
    FTDI_REQTYPE_OUT: lus.LIBUSB_REQUEST_TYPE_VENDOR |
        lus.LIBUSB_RECIPIENT_DEVICE |
        lus.LIBUSB_ENDPOINT_OUT,
    FTDI_IFC_A: 1,
    FTDI_CTL_RESET:         0,
    FTDI_CTL_SET_BITMODE:   0x0b,
    FTDI_CTL_SET_EVENT_CH:  0x06,
    FTDI_CTL_SET_ERROR_CH:  0x07
};

console.log(ftd.FTDI_REQTYPE_OUT);
let dev;

// descr.idProduct === 0x8a99
// 0x8a99 =
// 0x6014 = FTDI C232HM
// 0x6010

usb.getDeviceList().some(curdev => {
    const descr = curdev.deviceDescriptor;
    if (descr.idVendor === 0x0403) {
        dev = curdev;
        return true;
    }
});

dev.open();

const if0 = dev.interfaces[0];
console.log(if0);
const ep_in = if0.endpoints[0]; // (0x81);
const ep_out = if0.endpoints[1]; //(0x02);

if (if0.isKernelDriverActive()) {
    if0.detachKernelDriver();
}
if0.claim();

// console.log(dev.interfaces);

// dev.attachKernelDriver();

// ftdi reset

const p1 = new Promise((resolve, reject) => {
    console.log('opened');
});

const cfgOut = (bRequest, wValue) => new Promise((resolve, reject) =>
    dev.controlTransfer(
        ftd.FTDI_REQTYPE_OUT,
        bRequest,
        wValue,
        ftd.FTDI_IFC_A,
        Buffer.from([]),
        (err, data) => {
            console.log(err, data);
            resolve();
        }
    ));

const usbBulk = (buf) => new Promise((resolve, reject) =>
    ep_out.transfer(buf, err => {
        console.log(err);
        resolve();
    })
);

p1
    .then(cfgOut(ftd.FTDI_CTL_RESET, 0))
    .then(cfgOut(ftd.FTDI_CTL_SET_BITMODE, 0x0000))
    .then(cfgOut(ftd.FTDI_CTL_SET_BITMODE, 0x0200))
    .then(cfgOut(ftd.FTDI_CTL_SET_EVENT_CH, 0))
    .then(cfgOut(ftd.FTDI_CTL_SET_ERROR_CH, 0))
    .then(usbBulk(Buffer.from([
        0x85, // loopback off
        0x8a, // disable clock/5
        0x97, // Turn off adaptive clocking (may be needed for ARM)
        0x8D, // Disable three-phase clocking
        0x86, 0x00, 0x08, // set divisor 0x0800
        0x80, 0xe8, 0xeb, // set low state and dir
    ]
        .concat(lib.mv.any.IDLE)
        .concat([0x10, 0x02, 0x00, /* posedge, len=2 */ 0x55, 0x80, 0x14])
    )))
    .then(() => { dev.close(); })

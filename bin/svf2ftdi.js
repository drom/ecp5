#!/usr/bin/env node
'use strict';

const svf = require('../lib/svf');
// const chopper = require('../lib/stream-chopper');

const s1 = svf.parse();

process.stdin
    .setEncoding('ascii')
    // .pipe(chopper(1, 100))
    .pipe(s1)
    .pipe(process.stdout);

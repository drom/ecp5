#!/usr/bin/env node
'use strict';

const chopper = require('../lib/stream-chopper');

process.stdin
    .setEncoding('ascii')
    .pipe(chopper(1, 500))
    .pipe(process.stdout);

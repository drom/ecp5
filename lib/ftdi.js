'use strict';

const usb = require('usb');
// const sleep = require('util').promisify(setTimeout);
const mv = require('./jtag-state-change');
const initMPSSE = require('./init-mpsse');
const fdiv = require('./ftdi-mpsse-divider');
const ftd = require('./ftdi-flags');
const color = require('./colors');

const invHexStr = (str) => {
    if (str.length & 0x1) {
        throw new Error('uneven number of nibles in the HEX string');
    }
    return str.match(/.{1,2}/g).reverse().join('');
};

module.exports = () => {
    let dev, epIn, epOut;

    const cfgOut = (bRequest, wValue) =>
        new Promise(resolve =>
            dev.controlTransfer(
                ftd.FTDI_REQTYPE_OUT,
                bRequest,
                wValue,
                ftd.FTDI_IFC_A,
                Buffer.from([]),
                err => {
                    if (err) { throw err; }
                    resolve();
                }
            ));

    const datOut = buf => {
        if (buf === undefined) { throw new Error(); }
        // console.log(color.FgGreen, buf, color.Reset);
        return new Promise(resolve => {
            epOut.transfer(buf, err => {
                if (err) { throw err; }
                resolve();
            });
        });
    };

    const datInp = len => new Promise(resolve => {
        epIn.transfer(len, (err, dat) => {
            if (err) { throw err; }
            console.log(color.FgRed, dat, color.Reset);
            resolve();
        });
    });

    const close = async () => {
        // await datOut([0x80, 0, 0, 0x82, 0, 0]);
        dev.close();
        console.log('\nclose');
    };

    const init = async () => {
        await cfgOut(ftd.FTDI_CTL_RESET, 0);
        await cfgOut(ftd.FTDI_CTL_SET_BITMODE, 0x0000);
        await cfgOut(ftd.FTDI_CTL_SET_BITMODE, 0x0200);
        await cfgOut(ftd.FTDI_CTL_SET_EVENT_CH, 0);
        await cfgOut(ftd.FTDI_CTL_SET_ERROR_CH, 0);
        await datOut(fdiv(5e6).concat(initMPSSE));
    };

    const open = async () => {
        console.log('open');

        usb.getDeviceList().some(curdev => {
            const descr = curdev.deviceDescriptor;
            if (descr.idVendor === 0x0403) {
                dev = curdev;
                return true;
            }
        });

        dev.open();

        const if0 = dev.interfaces[0];
        epIn = if0.endpoints[0]; // (0x81);
        epOut = if0.endpoints[1]; //(0x02);

        if (if0.isKernelDriverActive()) {
            if0.detachKernelDriver();
        }

        if0.claim();

        await init();
    };

    const TMS = async (from, to) => {
        // console.log('TMS', from, to);
        if (from !== to) {
            await datOut(mv[from][to]);
        }
    };

    const TCK = async bitLen => {
        const byteLen = bitLen >> 3;
        const bitHead = bitLen & 0x7;
        if (bitHead > 0) {
            // console.log('tck', bitHead);
            let buf = Buffer.from('130000', 'hex');
            buf[1] = bitHead - 1;
            await datOut(buf);
        }

        if (byteLen > 0) {
            // console.log('TCK', byteLen);
            let buf = Buffer.alloc(byteLen + 3);
            buf[0] = 0x11;
            buf[1] = (byteLen - 1) & 0xff;
            buf[2] = ((byteLen - 1) >> 8) & 0xff;
            await datOut(buf);
        }
    };

    const TDI = async (data, bitLen, io, ext) => {
        if (bitLen > 0) {
            const maxBufLen = 0x10000;
            const cmd1 = io ? '39' : '19';
            const cmd2 = io ? '6b' : '6a';
            while(bitLen > (7 + ext)) {
                let buf;
                if (bitLen > (0xffff << 3)) {
                    const part = data.slice(-(maxBufLen << 1));
                    data = data.slice(0, -(maxBufLen << 1));
                    bitLen -= (maxBufLen << 3);
                    console.log('+', data.length / 2, part.length / 2, bitLen / 8);
                    buf = Buffer.from(cmd1 + 'ffff' + invHexStr(part), 'hex');
                } else {
                    const byteLen = (bitLen - ext) >> 3;
                    const part = data; // .slice(2); // FIXME ???
                    bitLen -= (byteLen << 3);
                    console.log('|', part.length / 2, bitLen / 8, byteLen);
                    buf = Buffer.from(cmd1 + '0000' + invHexStr(part), 'hex');
                    buf[1] = (byteLen - 1) & 0xff;
                    buf[2] = ((byteLen - 1) >> 8) & 0xff;
                }
                // console.log('TDI', io, buf);
                await datOut(buf);
            }
            const lastChars = data.slice(0, 2);
            const lastByte = parseInt(lastChars, 16);
            const bitSeq = (bitLen > 1) ? ('1B0' + (bitLen - 2).toString(16) + lastChars) : '';
            const lastBit = lastByte >> ((bitLen - 1) & 0x7);
            const extSeq = ext ? (cmd2 + '01' + (lastBit ? '81' : '01')) : '';
            const seq = bitSeq + extSeq;
            if (seq.length > 0) {
                const buf = Buffer.from(bitSeq + extSeq, 'hex');
                // console.log('tdi', io, buf);
                await datOut(buf);
            }
            if (io) {
                await datInp(0x1000);
            }
        } else {
            console.log('Zerro!!!');
        }
    };

    return {
        open: open,
        close: close,
        // datOut: datOut,
        // datInp: datInp,
        TMS: TMS,
        TCK: TCK,
        TDI: TDI
    };
};

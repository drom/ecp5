'use strict';

module.exports = [
    '0x00', // repeat 0x0
    'STATE',    // 1
    'SIR',      // 2 + len
    'SDR',      // 3 + len
    'TCK',      // 4 + count
    'SEC',      // 5 + 0 0 0
    '',
    '',
    'HIR', // 8
    'TIR', // 9
    'HDR', // A
    'TDR', // B
    '',
    '',
    'TDI', // E
    '',
    '',
    'TDO', // 11
    'MASK', // 12
    '',
    '',
    '',
    '',
    '',
    '',
    '',
    '',
    '0xFF' // FF = repeat 0xF
];

'use strict';

const stream = require('stream');
const interpreter = require('./interpreter');

function setup (s) {
    s.state = 'space';
    s.log = function (str) { s.push(Buffer.from(str)); };
    s.on('error', err => {
        s.log('on error');
        throw err;
    });
    interpreter(s);
    s._read = function () {};
}

exports.parse = function () {
    const s = stream.Duplex();
    setup(s);
    return s;
};

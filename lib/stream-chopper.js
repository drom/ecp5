'use strict';

const stream = require('stream');

// chooping input stream into multiple chunks of random (min =<length =< max)
function chopper (min, max) {
    const s = stream.Duplex();
    s.on('error', err => {
        throw err;
    });
    s._write = function (chunk, enc, next) {
        while(true) {
            const len = Math.round(Math.random() * (max - min) + min);
            if (chunk.length >= len) {
                // console.error(len);
                const head = chunk.slice(0, len);
                chunk = chunk.slice(len);
                s.push(head);
            } else if (chunk.length > 0) {
                // console.error(chunk.length);
                s.push(chunk);
                next();
                return;
            } else {
                next();
                return;
            }
        }
    };
    s._read = function () {};
    return s;
}

module.exports = chopper;

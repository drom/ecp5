## Programming

### HW

### SW

  * [Programming Tools User Guide - Lattice Semiconductor](http://www.latticesemi.com/-/media/LatticeSemi/Documents/UserManuals/MQ/ProgrammingToolsUserGuide31.ashx?document_id=50445)
  * https://github.com/swetland/jtag-mpsse

### FTDI
  * [Command Processor for MPSSE and MCU Host Bus Emulation Modes](http://www.ftdichip.com/Support/Documents/AppNotes/AN_108_Command_Processor_for_MPSSE_and_MCU_Host_Bus_Emulation_Modes.pdf)
  * [FTDI MPSSE Basics](http://www.ftdichip.com/Support/Documents/AppNotes/AN_135_MPSSE_Basics.pdf)

### File format

  * [SVF](https://en.wikipedia.org/wiki/Serial_Vector_Format) (serial vector format) = algorithm and data file in ASCII format
  * https://www.xjtag.com/about-jtag/svf-files/
  * http://www.jtagtest.com/pdf/svf_specification.pdf
  * VME = compressed SVF
